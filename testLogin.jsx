import axios from 'axios'
import "./login.scss"
import Cookies from 'js-cookies'
import React, { useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import { UserContext } from '../../context/UserContext'
import { Button, Checkbox, Form, Input } from 'antd';


const Login = () => {

  const Navigate = useNavigate()

  const { loginStatus, setLoginStatus } = useContext(UserContext)

  const [input, setInput] = useState({
    userName: "",
    password: ""
  })

  const handleChange = (e) => {
    let typeOfInput = e.target.value
    let name = e.target.name

    setInput({ ...input, [name]: typeOfInput })
  }

  const onFinish = (values) => {
    console.log('Success:', values);
    axios.post('https://flask-project-development.herokuapp.com/api/login', {
      userName: input.userName,
      password: input.password
    }).then((res) => {
      let token = res.data.data.access_token
      let users = res.data.data.users

      Cookies.setItem('access_token', token, { expires: 1 })
      Cookies.setItem('mitra_id', users.mitra_id, { expires: 1 })
      Cookies.setItem('name', users.name, { expires: 1 })
      Cookies.setItem('userName', users.userName, { expires: 1 })
      Cookies.setItem('wilayah_id', users.wilayah_id, { expires: 1 })
      Navigate('/')


      // console.log(res)
    }).catch(error => {
      if (error.response) {
        console.log(error.response);
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  // const handleSubmit = () => {




  //   // let authData = userList.find(x => x.name === input.name && x.password === input.password)

  //   // if(authData){ 
  //   //   setLoginStatus(true)
  //   //   Cookies.setItem('username', authData.username, {expires : 1})
  //   //   Cookies.setItem('password', authData.password, {expires : 1})
  //   // }else{
  //   //   alert('salah')
  //   // }
  // }


  return (
    <>
      <div className="login">
        <div className="loginForm">
          <Form
            name='basic'
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="on"
          >
            <Form.Item
              label="Username"
              name="userName"
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}
            >
              <Input/>
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password/>
            </Form.Item>

            {/* <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Checkbox>Remember me</Checkbox>
            </Form.Item> */}

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>

          {/* <form onSubmit={handleSubmit}>
            <label>Username:</label>
            <br />
            <input type="text" name='userName' value={input.userName} onChange={handleChange} />
            <br />
            <label>Password:</label>
            <br />
            <input type="password" name='password' value={input.password} onChange={handleChange} />
            <br />
            <br />
            <input type="submit" />
          </form> */}

        </div>
      </div>
    </>
  )
}

export default Login