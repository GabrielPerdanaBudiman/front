// import { Link } from "react-router-dom"
import { Tag } from 'antd';
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
// import { useState } from "react"

//user
export const userColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "name", title: "User Name", id: 'id', width: "20%", 
        // render: (params) => {
        //     return (
        //     <div className="cellWithImg">
        //         <img className="cellImg" src={params.row.img} alt="avatar" />
        //         {params.row.name}
        //     </div>
        //     );
        // },
    },
    
    {
        dataIndex: "img", title: "Gambar", id: 'id', width: '2.5%', render: img => {
            return (
                <div className="cellWithImg">
                    <img className="cellImg" src={img} alt={"img"} />
                    {/* {name} */}
                </div>
            )
        }
    },
    { dataIndex: "email", title: "Email", id: 'id', width: "30%" },
    { dataIndex: "age", title: "Age", id: 'id', width: "15%" },
    // {
    //     dataIndex: "status", title: "Status", id: 'id', width: "20%",
    //     renderCell: (params) => {
    //         return <div className={`cellWithStatus ${params.row.status}`}>{params.row.status}</div>
    //     }
    // },
    {
        title: 'Status',
        key: 'id',
        dataIndex: 'tags',
        width: "15%",
        render: (_, { tags }) => (
          <>
            {tags.map((tag) => {
              let color = tag.length > 5 ? 'geekblue' : 'green';
    
              if (tag === 'Admin') {
                color = 'volcano';
              } else {
                color = 'geekblue';
              }
    
              return (
                <Tag color={color} key={tag}>
                  {tag.toUpperCase()}
                </Tag>
              );
            })}
          </>
        ),
      },
      {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]


export const userRows = [
    {
        id: 1,
        name: "Test 1",
        img: "https://www.kindpng.com/picc/m/235-2350646_login-user-name-user-avatar-svg-hd-png.png",
        tags: ["Admin"],
        email: "test1@mail.com",
        age: 55,

    },
    {
        id: 2,
        name: "Test 2",
        img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        tags: ["Admin MPE"],
        email: "test2@mail.com",
        age: 45,

    },
    {
        id: 3,
        name: "Test 3",
        img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        tags: ["Admin APL"],
        email: "test3@mail.com",
        age: 28,

    },
    {
        id: 4,
        name: "Test 4",
        img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        tags: ["Admin PHS"],
        email: "test4@mail.com",
        age: 32,

    },
    {
        id: 5,
        name: "Test 5",
        img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        tags: ["Admin BPP"],
        email: "test5@mail.com",
        age: 29,

    },
    {
        id: 6,
        name: "Test 6",
        img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        tags: ["Admin SAM"],
        email: "test6@mail.com",
        age: 45,

    },
    {
        id: 7,
        name: "Test 7",
        img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        tags: ["Admin BHL"],
        email: "test7@mail.com",
        age: 35,

    },
]

//pupuk
export const pupukColumn = [
    // {
    //     dataIndex: "img", title: "Gambar", id: 'id', width: "10%", render: img => {
    //         return (
    //             <div className="cellWithImg">
    //                 <img className="cellImg" src={img} alt={"img"} />
    //             </div>
    //         )
    //     }
    // },
    { dataIndex: "name", title: "Nama Pupuk", width: 150 },
    // {
    //     dataIndex: "pupuk", title: "Pupuk", width: 230, renderCell: (params) => {
    //         return (
    //             <div className="cellWithImg">
    //                 <img className="cellImg" src={params.row.img} alt="avatar" />
    //                 {params.row.produk}
    //             </div>
    //         )
    //     }
    // },
    // { dataIndex: "stok", title: "Stok", width: "30%" },
    { dataIndex: "description", title: "Deskripsi", width: 150 },
    { dataIndex: "category_name", title: "Kategori", width: 150 },
    { dataIndex: "type", title: "Tipe", width: 150 },
    { dataIndex: "uom_name", title: "Satuan", width: 150 },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: 150 ,
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]


export const pupukRows = [
    {
        id: 1,
        pupuk: "Super Dolomite",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1000"
        satuan: "kg"

    },
    {
        id: 2,
        pupuk: "CP 15.15.6.4",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1200"
        satuan: "kg"


    }, {
        id: 3,
        pupuk: "NPK Mg 13.6.27.4",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1100"
        satuan: "kg"


    }, {
        id: 4,
        pupuk: "CP 12.12.17.2+Te",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "2000"
        satuan: "kg"


    }, {
        id: 5,
        pupuk: "Rock Phosphate",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 6,
        pupuk: "Copper Sulphate",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 7,
        pupuk: "Microsil",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 8,
        pupuk: "CP 13.6.27+1,25 TE",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 9,
        pupuk: "CP 15.8.23+1,0 Te",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 10,
        pupuk: "CP 12.12.17.2+0,65B",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 11,
        pupuk: "Kaptan",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 12,
        pupuk: "Urea",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 13,
        pupuk: "KCL",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 14,
        pupuk: "Borate",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 15,
        pupuk: "Zinc Sulphate",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 16,
        pupuk: "Kieserite",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 17,
        pupuk: "CP 13.6.27",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 18,
        pupuk: "TSP",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },{
        id: 19,
        pupuk: "Bayfolan",
        // img: "https://www.nicepng.com/png/full/138-1388174_login-account-icon.png",
        // stok: "1400"
        satuan: "kg"


    },
]

//pemilik

export const pemilikColumn = [
    { dataIndex: "name", title: "Nama Perusahaan", width: 150 },
    { dataIndex: "email", title: "Email", width: 150 },
    { dataIndex: "phone", title: "No Telp", width: 150 },
    {
        title: 'Action',
        dataIndex: 'id',
        width: 150,
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const pemilikRows = [
    {
        id: 1,
        idPemilik: "MPE",
        nama: "Multi Prima Entakai, PT"
    },
    {
        id: 2,
        idPemilik: "APL",
        nama: "Agro Prima Lestari, PT"
    },
    {
        id: 3,
        idPemilik: "PHS",
        nama: "Permata Hijau Sarana, PT"
    },
    {
        id: 4,
        idPemilik: "BPP",
        nama: "Bukit Prima Plantindo, PT"
    },
    {
        id: 5,
        idPemilik: "SAM",
        nama: "Sintang Agro Mandiri, PT"
    },
    {
        id: 6,
        idPemilik: "BHL",
        nama: "Bukit Hijau Lestari, PT"
    },
]

//mitra

export const mitraColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "idKelompok", title: "ID Kelompok", id: 'id', width: "22.5%" },
    { dataIndex: "nama", title: "Nama Mitra", id: 'id', width: "60%" },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const mitraRows = [
    {
        id: 1,
        idKelompok: "I",
        nama: "Inti"
    },
    {
        id: 2,
        idKelompok: "P",
        nama: "Plasma"
    },
    {
        id: 3,
        idKelompok: "K",
        nama: "KSB"
    }
]

//wilayah

export const wilayahColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "idWilayah", title: "ID Wilayah", id: 'id', width: "22.5%" },
    { dataIndex: "nama", title: "Nama Wilayah", id: 'id', width: "60%" },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const wilayahRows = [
    {
        id: 1,
        idWilayah: "",
        nama: "Barat"
    },
    {
        id: 2,
        idWilayah: "",
        nama: "Timur"
    },
    {
        id: 3,
        idWilayah: "",
        nama: "Tengah"
    }
]

//kapasitas

export const kapasitasColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "idGudang", title: "ID Gudang", id: 'id', width: "10%" },
    { dataIndex: "kapasitas", title: "Kapasitas", id: 'id', width: "62.5%" },
    { dataIndex: "satuan", title: "Satuan", id: 'id', width: "10%" },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const kapasitasRows = [
    {
        id: 1,
        idGudang: "GIGS",
        kapasitas: "200",
        satuan: "Kg",
    },
    {
        id: 2,
        idGudang: "GSRA",
        kapasitas: "1050",
        satuan: "Kg",
    },
    {
        id: 3,
        idGudang: "GSLM",
        kapasitas: "400",
        satuan: "Kg",
    },
    {
        id: 4,
        idGudang: "GTLP",
        kapasitas: "650",
        satuan: "Kg",
    },
    {
        id: 5,
        idGudang: "GSBT",
        kapasitas: "350",
        satuan: "Kg",
    },
    {
        id: 6,
        idGudang: "GGNS",
        kapasitas: "550",
        satuan: "Kg",
    },
    {
        id: 7,
        idGudang: "GSPK",
        kapasitas: "700",
        satuan: "Kg",
    },
    {
        id: 8,
        idGudang: "GSKP",
        kapasitas: "450",
        satuan: "Kg",
    },
    {
        id: 9,
        idGudang: "GSGR",
        kapasitas: "0",
        satuan: "Kg",
    },
    {
        id: 10,
        idGudang: "GJRR",
        kapasitas: "900",
        satuan: "Kg",
    },
    {
        id: 11,
        idGudang: "GTKG",
        kapasitas: "500",
        satuan: "Kg",
    },
    {
        id: 12,
        idGudang: "GSJK",
        kapasitas: "400",
        satuan: "Kg",
    },
    {
        id: 13,
        idGudang: "GKMT",
        kapasitas: "400",
        satuan: "Kg",
    },
    {
        id: 14,
        idGudang: "GBLH",
        kapasitas: "600",
        satuan: "Kg",
    },
    {
        id: 15,
        idGudang: "GTSS",
        kapasitas: "500",
        satuan: "Kg",
    },
    {
        id: 16,
        idGudang: "GPAO",
        kapasitas: "400",
        satuan: "Kg",
    },
    {
        id: 17,
        idGudang: "GPGY",
        kapasitas: "80",
        satuan: "Kg",
    },
]

//gudang

export const gudangColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "idGudang", title: "ID Gudang", id: 'id', width: "15%" },
    { dataIndex: "lokasi", title: "Lokasi", id: 'id', width: "15%" },
    { dataIndex: "pt", title: "PT", id: 'id', width: "15%" },
    { dataIndex: "wilayah", title: "Wilayah", id: 'id', width: "15%" },
    { dataIndex: "kelompok", title: "Kelompok", id: 'id', width: "22.5%" },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const gudangRows = [
    {
        id: 1,
        idGudang: "GIGS",
        lokasi: "IGS / Serasi",
        pt: "MPE",
        wilayah: "Barat",
        kelompok: "Test 1",
    },
    {
        id: 2,
        idGudang: "GSRA",
        lokasi: "SRA",
        pt: "MPE",
        wilayah: "Timur",
        kelompok: "Test 2",
    },
    {
        id: 3,
        idGudang: "GSLM",
        lokasi: "SLM",
        pt: "MPE",
        wilayah: "Tengah",
        kelompok: "Test 3",
    },
    {
        id: 4,
        idGudang: "GTLP",
        lokasi: "TLP",
        pt: "MPE",
        wilayah: "Barat",
        kelompok: "Test 4",
    },
    {
        id: 5,
        idGudang: "GSBT",
        lokasi: "Sebatri",
        pt: "APL",
        wilayah: "Timur",
        kelompok: "Test 5",
    },
    {
        id: 6,
        idGudang: "GGNS",
        lokasi: "GNS",
        pt: "PHS",
        wilayah: "Tengah",
        kelompok: "Test 6",
    },
    {
        id: 7,
        idGudang: "GSPK",
        lokasi: "SPK",
        pt: "PHS",
        wilayah: "Barat",
        kelompok: "Test 7",
    },
    {
        id: 8,
        idGudang: "GSKP",
        lokasi: "SKP",
        pt: "PHS",
        wilayah: "Timur",
        kelompok: "Test 8",
    },
    {
        id: 9,
        idGudang: "GSGR",
        lokasi: "Sungai Raya",
        pt: "PHS",
        wilayah: "Tengah",
        kelompok: "Test 9",
    },
    {
        id: 10,
        idGudang: "GJRR",
        lokasi: "Jerora",
        pt: "BPP",
        wilayah: "Barat",
        kelompok: "Test 10",
    },
    {
        id: 11,
        idGudang: "GTKG",
        lokasi: "Tekang",
        pt: "BPP",
        wilayah: "Timur",
        kelompok: "Test 11",
    },
    {
        id: 12,
        idGudang: "GSJK",
        lokasi: "Sejongkong",
        pt: "BPP",
        wilayah: "Tengah",
        kelompok: "Test 12",
    },
    {
        id: 13,
        idGudang: "GKMT",
        lokasi: "Klamentia",
        pt: "SAM",
        wilayah: "Barat",
        kelompok: "Test 13",
    },
    {
        id: 14,
        idGudang: "GBLH",
        lokasi: "Balai Harapan / Mulung",
        pt: "SAM",
        wilayah: "Timur",
        kelompok: "Test 14",
    },
    {
        id: 15,
        idGudang: "GTSS",
        lokasi: "Tohsampuas",
        pt: "SAM",
        wilayah: "Tengah",
        kelompok: "Test 15",
    },
    {
        id: 16,
        idGudang: "GPAO",
        lokasi: "PAO / Parisukasari",
        pt: "BHL",
        wilayah: "Barat",
        kelompok: "Test 16",
    },
    {
        id: 17,
        idGudang: "GPGY",
        lokasi: "Pagaraya",
        pt: "BHL",
        wilayah: "Timur",
        kelompok: "Test 17",
    }
]

//bensin

export const bensinColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "bensin", title: "Jenis Bensin", id: 'id', width: "52.5%" },
    { dataIndex: 'harga', title: 'Harga', id: 'id', width: '15%' },
    { dataIndex: 'satuan', title: 'Satuan', id: 'id', width: '15%' },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const bensinRows = [
    {
        id: 1,
        bensin: "Pertalite",
        harga: "Rp. 7.650",
        satuan: "Liter"
    },
    {
        id: 2,
        bensin: "Pertamax",
        harga: "Rp. 12.500",
        satuan: "Liter"
    },
    {
        id: 3,
        bensin: "Pertamax Turbo",
        harga: "Rp. 16.200",
        satuan: "Liter"
    },
    {
        id: 4,
        bensin: "Dex",
        harga: "Rp. 16.500",
        satuan: "Liter"
    },
    {
        id: 5,
        bensin: "Dexlite",
        harga: "Rp. 15.000",
        satuan: "Liter"
    },
]

//kendaraan

export const kendaraanColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "kendaraan", title: "Jenis Kendaraan", id: 'id', width: "42.5%" },
    { dataIndex: 'nopol', title: 'Nomor Polisi', id: 'id', width: '15%' },
    { dataIndex: 'kmawal', title: 'Km Awal', id: 'id', width: '15%' },
    { dataIndex: 'tgl', title: 'Tanggal', id: 'id', width: '10%' },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const kendaraanRows = [
    {
        id: 1,
        kendaraan: "Toyota New Dyna",
        nopol: "B0001TST",
        kmawal: "0",
        tgl: "1"
    },
    {
        id: 2,
        kendaraan: "Mitsubishi Fuso Euro",
        nopol: "B0002TST",
        kmawal: "0",
        tgl: "2"
    },
    {
        id: 3,
        kendaraan: "Isuzu Traga",
        nopol: "B0003TST",
        kmawal: "0",
        tgl: "3"
    },
]

//test

export const testColumn = [
    { dataIndex: 'id', title: 'ID', id: 'id', width: '7.5%' },
    { dataIndex: "idGudang", title: "ID Gudang", id: 'id', width: "12.5%" },
    { dataIndex: "lokasi", title: "Lokasi", id: 'id', width: "20%" },
    { dataIndex: "pt", title: "PT", id: 'id', width: "15%" },
    { dataIndex: "wilayah", title: "Wilayah", id: 'id', width: "15%" },
    { dataIndex: "kelompok", title: "Kelompok", id: 'id', width: "20%" },
    {
        title: 'Action',
        id: 'id', 
        dataIndex: 'id',
        width: "10%",
        render: () => {
            return (
                <>
                    <EditOutlined
                        onClick={() => {
                            
                        }}
                    />
                    <DeleteOutlined
                        onClick={() => {
                            
                        }}
                        style={{ color: "red", marginLeft: 12 }}
                    />
                </>
            );
        }
    },
]

export const testRows = [
    {
        id: 1,
        idGudang: "GIGS",
        lokasi: "IGS / Serasi",
        pt: "MPE",
        wilayah: "Barat",
        kelompok: "Test 1",
    },
    {
        id: 2,
        idGudang: "GSRA",
        lokasi: "SRA",
        pt: "MPE",
        wilayah: "Timur",
        kelompok: "Test 2",
    },
    {
        id: 3,
        idGudang: "GSLM",
        lokasi: "SLM",
        pt: "MPE",
        wilayah: "Tengah",
        kelompok: "Test 3",
    },
    {
        id: 4,
        idGudang: "GTLP",
        lokasi: "TLP",
        pt: "MPE",
        wilayah: "Barat",
        kelompok: "Test 4",
    },
    {
        id: 5,
        idGudang: "GSBT",
        lokasi: "Sebatri",
        pt: "APL",
        wilayah: "Timur",
        kelompok: "Test 5",
    },
    {
        id: 6,
        idGudang: "GGNS",
        lokasi: "GNS",
        pt: "PHS",
        wilayah: "Tengah",
        kelompok: "Test 6",
    },
    {
        id: 7,
        idGudang: "GSPK",
        lokasi: "SPK",
        pt: "PHS",
        wilayah: "Barat",
        kelompok: "Test 7",
    },
    {
        id: 8,
        idGudang: "GSKP",
        lokasi: "SKP",
        pt: "PHS",
        wilayah: "Timur",
        kelompok: "Test 8",
    },
    {
        id: 9,
        idGudang: "GSGR",
        lokasi: "Sungai Raya",
        pt: "PHS",
        wilayah: "Tengah",
        kelompok: "Test 9",
    },
    {
        id: 10,
        idGudang: "GJRR",
        lokasi: "Jerora",
        pt: "BPP",
        wilayah: "Barat",
        kelompok: "Test 10",
    },
    {
        id: 11,
        idGudang: "GTKG",
        lokasi: "Tekang",
        pt: "BPP",
        wilayah: "Timur",
        kelompok: "Test 11",
    },
    {
        id: 12,
        idGudang: "GSJK",
        lokasi: "Sejongkong",
        pt: "BPP",
        wilayah: "Tengah",
        kelompok: "Test 12",
    },
    {
        id: 13,
        idGudang: "GKMT",
        lokasi: "Klamentia",
        pt: "SAM",
        wilayah: "Barat",
        kelompok: "Test 13",
    },
    {
        id: 14,
        idGudang: "GBLH",
        lokasi: "Balai Harapan / Mulung",
        pt: "SAM",
        wilayah: "Timur",
        kelompok: "Test 14",
    },
    {
        id: 15,
        idGudang: "GTSS",
        lokasi: "Tohsampuas",
        pt: "SAM",
        wilayah: "Tengah",
        kelompok: "Test 15",
    },
    {
        id: 16,
        idGudang: "GPAO",
        lokasi: "PAO / Parisukasari",
        pt: "BHL",
        wilayah: "Barat",
        kelompok: "Test 16",
    },
    {
        id: 17,
        idGudang: "GPGY",
        lokasi: "Pagaraya",
        pt: "BHL",
        wilayah: "Timur",
        kelompok: "Test 17",
    }
]

