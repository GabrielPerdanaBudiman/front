import React, { useState, createContext } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import axios from "axios"
import Cookies from 'js-cookies'
import moment from 'moment';
import 'moment/locale/zh-cn';



export const AppContext = createContext()

export const AppProvider = props => {

    //Component-----------------------------------------------------------------------------------------------

    const Navigate = useNavigate()
    const dateFormat = ['DD-MM-YYYY'];

    let {id} = useParams()
    const [loginStatus, setLoginStatus] = useState(false)
    const [fetchStatus, setFetchStatus] = useState(true)
    // const [dataUser, setDataUser] = useState([])
    const [dataPupuk, setDataPupuk] = useState([])
    const [dataPemilik, setDataPemilik] = useState([])
    const [dataStockPicking, setDataStockPicking] = useState([])
    const [dataStockPickingEdit, setDataStockPickingEdit] = useState([])
    const [dataStockPickingType, setDataStockPickingType] = useState([])
    const [dataStockLocation, setDataStockLocation] = useState([])


    //Component-----------------------------------------------------------------------------------------------

    //Login & Logout------------------------------------------------------------------------------------------
    const logon = (values) => {
        console.log('Success:', values);
        axios.post('http://127.0.0.1:5000/api/login', {
            userName: values.userName,
            password: values.password
        })
            .then((res) => {
                let token = res.data.data.access_token
                let users = res.data.data.users

                Cookies.setItem('access_token', token, { expires: 1 })
                Cookies.setItem('mitra_id', users.mitra_id, { expires: 1 })
                Cookies.setItem('name', users.name, { expires: 1 })
                Cookies.setItem('userName', users.userName, { expires: 1 })
                Cookies.setItem('wilayah_id', users.wilayah_id, { expires: 1 })
                Navigate('/')

            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    };

    const logonFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const handleLogout = () => {
        setLoginStatus(false)
        Cookies.removeItem('access_token')
        Cookies.removeItem('mitra_id')
        Cookies.removeItem('name')
        Cookies.removeItem('userName')
        Cookies.removeItem('wilayah_id')
        Navigate('/login')
    }
    //Login & Logout------------------------------------------------------------------------------------------

    //Headers-------------------------------------------------------------------------------------------------
    let headers = {
        "Authorization": "Bearer " + Cookies.getItem('access_token')
    }
    //Headers-------------------------------------------------------------------------------------------------

    //Users---------------------------------------------------------------------------------------------------
    // const fetchDataUser = async () => {
    //     await axios.get('http://127.0.0.1:5000/api/company',
    //         { headers }
    //     )
    //         .then(response => setDataUser(response.data.data.company))
    //         .catch(error => {
    //             if (error.response) {
    //                 console.log(error.response);
    //             }
    //         });
    // }
    //Users---------------------------------------------------------------------------------------------------


    //Pupuk Master -------------------------------------------------------------------------------------------
    const fetchDataPupuk = async () => {
        await axios.get('http://127.0.0.1:5000/api/products',
            { headers }
        )
            .then(response => setDataPupuk(response.data.data.products))
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }
    //Pupuk Master -------------------------------------------------------------------------------------------

    //Pemilik Master -----------------------------------------------------------------------------------------
    const fetchDataPemilik = async () => {
        await axios.get('http://127.0.0.1:5000/api/company',
            { headers }
        )
            .then(response => setDataPemilik(response.data.data.company))
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }
    //Pemilik Master -----------------------------------------------------------------------------------------

    //Stock Picking ------------------------------------------------------------------------------------------
    const fetchDataStockPicking = async () => {
        await axios.get('http://127.0.0.1:5000/api/stockpickings',
            { headers }
        )
            .then(response => setDataStockPicking(response.data.data.stockPickings))
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });

    }
    //Stock Picking-------------------------------------------------------------------------------------------

    //Stock Picking By ID-------------------------------------------------------------------------------------
    const fetchDataStockPickingById = async () => {
        await axios.get('http://127.0.0.1:5000/api/stockpickings/<int:id>',
            { headers }
        )
            .then(response => setDataStockPicking(response.data.data.stockPickings))
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });

    }
    //Stock Picking By ID-------------------------------------------------------------------------------------

    //Picking Type--------------------------------------------------------------------------------------------
    const fetchDataPickingType = async () => {
        await axios.get('http://127.0.0.1:5000/api/pickingtype',
            { headers }
        )
            .then(response => setDataStockPickingType(response.data.data.stockPickingType))
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }
    //Picking Type--------------------------------------------------------------------------------------------

    //Location------------------------------------------------------------------------------------------------
    const fetchDataLocation = async () => {
        await axios.get('http://127.0.0.1:5000/api/location',
            { headers }
        )
            .then(response => setDataStockLocation(response.data.data.stockLocation))
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }
    //Location------------------------------------------------------------------------------------------------

    //Option--------------------------------------------------------------------------------------------------
    const option = [
        {
            value: '1',
            label: 'True',
        },
        {
            value: '0',
            label: 'False',
        },
    ];
    //Option--------------------------------------------------------------------------------------------------

    //testData------------------------------------------------------------------------------------------------
    const stock_move_ids = [
        {
            demand: 0,
            done: 0,
            product_uom: 0,
            product_tmpl_id: 0,
        }
    ];
    //testData------------------------------------------------------------------------------------------------

    //StockPick-----------------------------------------------------------------------------------------------
    const StockPick = (values) => {
        console.log('Success:', values);

        let stockpick = [{
            date: moment(values.date).format("DD-MM-YYYY"),
            origin: values.origin,
            picking_type_id: parseInt(values.picking_type_id),
            location_id: parseInt(values.location_id),
            location_dest_id: parseInt(values.location_dest_id),
            stock_move_ids: [
                {
                    product_tmpl_id: parseInt(values?.stock_move_ids?.[0]?.product_tmpl_id),
                    demand: parseInt(values?.stock_move_ids?.[0]?.demand),
                    done: parseInt(values?.stock_move_ids?.[0]?.done),
                    product_uom: parseInt(values?.stock_move_ids?.[0]?.product_uom),
                },
            ],
        }];

        let params = JSON.stringify(stockpick)

        console.log(params)

        axios.post('http://127.0.0.1:5000/api/stockpickings', params, { headers })
            .then(() => {
                Navigate('/')
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });

    };

    const StockPickFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    //StockPick-----------------------------------------------------------------------------------------------






    let Function = {
        logon, logonFailed, handleLogout, fetchDataPupuk, fetchDataPemilik, fetchDataStockPicking,
        fetchDataPickingType, fetchDataLocation, fetchDataStockPickingById, option, stock_move_ids, StockPick, StockPickFailed
    }

    let State = {
        dateFormat, fetchStatus, setFetchStatus, loginStatus, setLoginStatus, dataPupuk, setDataPupuk, dataPemilik, setDataPemilik,
        dataStockPicking, setDataStockPicking, dataStockPickingType, setDataStockPickingType,
        dataStockLocation, setDataStockLocation, dataStockPickingEdit, setDataStockPickingEdit, id
    }

    return (
        <AppContext.Provider value={{ Function, State }}>
            {props.children}
        </AppContext.Provider>
    )

}