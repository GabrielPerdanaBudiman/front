import "./home.scss"
import Widget from "../../components/widgets/Widget"
import Featured from "../../components/featured/Featured"
import Chart from "../../components/chart/Chart"
import Maintable from "../../components/table/Table"

const Home = () => {
  return (
    <>
      <div className="home">
        <div className="widgets">
          <Widget type="masuk" />
          <Widget type="keluar" />
          <Widget type="anggaran" />
          <Widget type="pengeluaran" />
        </div>
        <div className="charts">
          {/* <Featured /> */}
          <Chart title="DATA PUPUK TAHUN 2020 Inti & Plasma" aspect={3/1} />
        </div>
        <div className="listContainer">
          <div className="listTitle">Latest Transactions</div>
          <Maintable />
        </div>
      </div>
    </>
  )
}

export default Home