import React, { useState, Fragment } from "react"
import "../table.scss"
import { nanoid } from "nanoid"
import { datanpb } from "../../../mock-data.js"
import Editnpb from "../../../components/readonlyrow/npb/Editnpb"
import { Table, Modal } from 'antd';
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";


const Npb = () => {
    const columns = [
        {
            title: 'Kode',
            width: 150,
            dataIndex: 'kode',
            key: '1',
            fixed: 'left',
        },
        {
            title: 'Nama Barang',
            width: 150,
            dataIndex: 'namaBarang',
            key: '2',
            fixed: 'left',
        },
        {
            title: 'Stok',
            dataIndex: 'stok',
            key: '3',
            width: 150,
        },
        {
            title: 'Part Number',
            dataIndex: 'partNumber',
            key: '4',
            width: 150,
        },
        {
            title: 'Pg/Pic',
            dataIndex: 'pgPic',
            key: '5',
            width: 150,
        },
        {
            title: 'Qty',
            dataIndex: 'qty',
            key: '6',
            width: 150,
        },
        {
            title: 'Koreksi',
            dataIndex: 'koreksi',
            key: '7',
            width: 150,
        },
        {
            title: 'Outstanding',
            dataIndex: 'outstanding',
            key: '8',
            width: 150,
        },
        {
            title: 'Terima',
            dataIndex: 'terima',
            key: '9',
            width: 150,
        },
        {
            title: 'Satuan',
            dataIndex: 'satuan',
            key: '10',
            width: 150,
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: '11',
            width: 150,
        },
        {
            title: 'Lokasi Beli',
            dataIndex: 'lokasiBeli',
            key: '12',
            width: 150,
        },
        {
            title: 'Keterangan',
            dataIndex: 'keterangan',
            key: '13',
            width: 150,
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (npbId) => {
                return (
                    <>
                        <EditOutlined
                            onClick={() => {
                                editOn(npbId);
                            }}
                        />
                        <DeleteOutlined
                            onClick={() => {
                                handleDeleteClick(npbId);
                            }}
                            style={{ color: "red", marginLeft: 12 }}
                        />
                    </>
                );
            }
        },
    ];

    const [npbs, setNpbs] = useState(datanpb)
    const [isEditing, setIsEditing] = useState(false);
    const [editNpbId, setEditNpbId] = useState(null)

    const [addFormData, setAddFormData] = useState({
        kode: "",
        namaBarang: "",
        stok: "",
        partNumber: "",
        pgPic: "",
        qty: "",
        koreksi: "",
        outstanding: "",
        terima: "",
        satuan: "",
        remark: "",
        lokasiBeli: "",
        keterangan: ""
    })

    const handleAddFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = { ...addFormData };
        newFormData[fieldName] = fieldValue;

        setAddFormData(newFormData);
    }

    const handleAddFormSubmit = (event) => {
        event.preventDefault();

        const newNpb = {
            id: nanoid(),
            kode: addFormData.kode,
            namaBarang: addFormData.namaBarang,
            stok: addFormData.stok,
            partNumber: addFormData.partNumber,
            pgPic: addFormData.pgPic,
            qty: addFormData.qty,
            koreksi: addFormData.koreksi,
            outstanding: addFormData.outstanding,
            terima: addFormData.terima,
            satuan: addFormData.satuan,
            remark: addFormData.remark,
            lokasiBeli: addFormData.lokasiBeli,
            keterangan: addFormData.keterangan
        }

        const newNpbs = [...npbs, newNpb];
        setNpbs(newNpbs);
        console.log(datanpb)
    }

    const handleDeleteClick = (npbId) => {

        Modal.confirm({
            title: "test",
            okText: "Ya",
            okType: "danger",
            onOk: () => {
                setNpbs((npbs) => {
                    return npbs.filter((npb) => npb.id !== npbId.id);
                });
            }
        })
    }

    const editOn = (npbId) => {
        setIsEditing(true);
        setEditNpbId({ ...npbId });
    };
    const resetEditing = () => {
        setIsEditing(false);
        setEditNpbId(null);
      };

    return (
        <>
            <div className="app-container">

                <div className="top">
                    Tambah Data Nota Pembelian Barang
                </div>

                <div className="bottom">

                    <form onSubmit={handleAddFormSubmit} className="formTop">
                        <div className="inputs">
                            <input
                                type="text"
                                name="kode"
                                required="required"
                                placeholder="Input Kode Barang.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="namaBarang"
                                required="required"
                                placeholder="Input Nama Barang.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="stok"
                                required="required"
                                placeholder="Input Jumlah Stok.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="partNumber"
                                required="required"
                                placeholder="Input Part Number.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="pgPic"
                                required="required"
                                placeholder="Input Pg/Pic.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="qty"
                                required="required"
                                placeholder="Input Qty.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="koreksi"
                                required="required"
                                placeholder="Input Koreksi.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="outstanding"
                                required="required"
                                placeholder="Input Outstanding.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="terima"
                                required="required"
                                placeholder="Input Terima.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="satuan"
                                required="required"
                                placeholder="Input Satuan.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="remark"
                                required="required"
                                placeholder="Input Remark.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="lokasiBeli"
                                required="required"
                                placeholder="Input Lokasi Beli.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="keterangan"
                                required="required"
                                placeholder="Input Keterangan.."
                                onChange={handleAddFormChange}
                            />
                        </div>
                        <button type="submit">Add</button>
                    </form>

                    <form className="formBottom">
                        <Table
                            columns={columns}
                            dataSource={npbs}
                            scroll={{
                                x: 1500,
                            }}
                            sticky
                        />
                        <Modal
                            title="Edit Npb"
                            visible={isEditing}
                            okText="Save"
                            onCancel={() => {
                                resetEditing();
                            }}
                            onOk={() => {
                                setNpbs((npbs) => {
                                    return npbs.map((npb) => {
                                        if (npb.id === editNpbId.id) {
                                            return editNpbId;
                                        } else {
                                            return npb;
                                        }
                                    });
                                });
                                resetEditing();
                            }}
                        >
                            <Editnpb 
                            editNpbId={editNpbId}
                            setEditNpbId={setEditNpbId}
                            />
                        </Modal>
                        {/* <table>
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Nama Barang</th>
                        <th>Stock</th>
                        <th>Qty</th>
                        <th>Lokasi Beli</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {npbs.map((npb) => (
                        <Fragment>
                            { editNpbId === npb.id ? (
                            <Editnpb 
                            editFormData={editFormData}
                            handleEditFormChange={handleEditFormChange}
                            handleCancleClick={handleCancleClick}
                            />
                            ) : (
                            <Readnpb 
                            npb={npb}
                            handleEditClick={handleEditClick} 
                            handleDeleteClick={handleDeleteClick}
                            />
                            )}
                        </Fragment>
                    ))}
                </tbody>
            </table> */}
                    </form>
                </div>

            </div>
        </>
    )
}

export default Npb