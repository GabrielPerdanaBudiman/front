import React, { useState, Fragment } from "react"
import "../table.scss"
import { nanoid } from "nanoid"
import { databtb } from "../../../mock-data.js"
import Editbtb from "../../../components/readonlyrow/btb/Editbtb"
import { Table, Modal } from 'antd';
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";


const Btb = () => {
    const columns = [
        {
            title: 'Kode',
            width: 150,
            dataIndex: 'kode',
            key: '1',
            fixed: 'left',
        },
        {
            title: 'Nama Barang',
            width: 150,
            dataIndex: 'namaBarang',
            key: '2',
            fixed: 'left',
        },
        {
            title: 'Qty',
            dataIndex: 'qty',
            key: '3',
            width: 150,
        },
        {
            title: 'Satuan',
            dataIndex: 'satuan',
            key: '4',
            width: 150,
        },
        {
            title: 'Harga Satuan',
            dataIndex: 'hargaSatuan',
            key: '5',
            width: 150,
        },
        {
            title: 'Valuta',
            dataIndex: 'valuta',
            key: '6',
            width: 150,
        },
        {
            title: 'Kurs',
            dataIndex: 'kurs',
            key: '7',
            width: 150,
        },
        {
            title: 'Jumlah',
            dataIndex: 'jumlah',
            key: '8',
            width: 150,
        },
        {
            title: 'NPB',
            dataIndex: 'npb',
            key: '9',
            width: 150,
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: '10',
            width: 150,
        },
        {
            title: 'Keterangan',
            dataIndex: 'keterangan',
            key: '11',
            width: 150,
        },
        {
            title: 'Akun',
            dataIndex: 'akun',
            key: '12',
            width: 150,
        },
        {
            title: 'Nama Akun',
            dataIndex: 'namaAkun',
            key: '13',
            width: 150,
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (btbId) => {
                return (
                    <>
                        <EditOutlined
                            onClick={() => {
                                editOn(btbId);
                            }}
                        />
                        <DeleteOutlined
                            onClick={() => {
                                handleDeleteClick(btbId);
                            }}
                            style={{ color: "red", marginLeft: 12 }}
                        />
                    </>
                );
            }
        },
    ];
    const codes = ['Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5']

    const [btbs, setBtbs] = useState(databtb)
    const [isEditing, setIsEditing] = useState(false);
    const [editBtbId, setEditBtbId] = useState(null)

    const [addFormData, setAddFormData] = useState({
        kode: "",
        namaBarang: "",
        qty: "",
        satuan: "",
        hargaSatuan: "",
        valuta: "",
        kurs: "",
        jumlah: "",
        npb: "",
        remark: "",
        keterangan: "",
        akun: "",
        namaAkun: ""
    })

    const handleAddFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = { ...addFormData };
        newFormData[fieldName] = fieldValue;

        setAddFormData(newFormData);
    }

    const handleAddFormSubmit = (event) => {
        event.preventDefault();

        const newBtb = {
            id: nanoid(),
            kode: addFormData.kode,
            namaBarang: addFormData.namaBarang,
            qty: addFormData.qty,
            satuan: addFormData.satuan,
            hargaSatuan: addFormData.hargaSatuan,
            valuta: addFormData.valuta,
            kurs: addFormData.kurs,
            jumlah: addFormData.jumlah,
            npb: addFormData.npb,
            remark: addFormData.remark,
            keterangan: addFormData.keterangan,
            akun: addFormData.akun,
            namaAkun: addFormData.namaAkun
        }

        const newBtbs = [...btbs, newBtb];
        setBtbs(newBtbs);
        console.log(databtb)
    }

    const handleDeleteClick = (btbId) => {

        Modal.confirm({
            title: "test",
            okText: "Ya",
            okType: "danger",
            onOk: () => {
                setBtbs((btbs) => {
                    return btbs.filter((btb) => btb.id !== btbId.id);
                });
            }
        })
    }

    const editOn = (btbId) => {
        setIsEditing(true);
        setEditBtbId({ ...btbId });
    };
    const resetEditing = () => {
        setIsEditing(false);
        setEditBtbId(null);
      };

    return (
        <>
            <div className="app-container">

                <div className="top">
                    Tambah Data Nota Pembelian Barang
                </div>

                <div className="bottom">

                    <form onSubmit={handleAddFormSubmit} className="formTop">
                        <div className="inputs">
                            <select 
                            name="kode"
                            required="required" 
                            onChange={handleAddFormChange}>
                                <option value="">Pilih Kode</option>
                                {codes.map((kode, index)=>{
                                    return <option key={index} value={kode}>{kode}</option>
                                })}
                            </select>
                            <input
                                type="text"
                                name="namaBarang"
                                required="required"
                                placeholder="Input Nama Barang.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="qty"
                                required="required"
                                placeholder="Input Qty.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="satuan"
                                required="required"
                                placeholder="Input Satuan.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="hargaSatuan"
                                required="required"
                                placeholder="Input Harga Satuan.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="valuta"
                                required="required"
                                placeholder="Input Valuta.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="kurs"
                                required="required"
                                placeholder="Input Kurs.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="jumlah"
                                required="required"
                                placeholder="Input Jumlah.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="npb"
                                required="required"
                                placeholder="Input NPB.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="remark"
                                required="required"
                                placeholder="Input Remark.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="keterangan"
                                required="required"
                                placeholder="Input Keterangan.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="akun"
                                required="required"
                                placeholder="Input Akun.."
                                onChange={handleAddFormChange}
                            />
                            <input
                                type="text"
                                name="namaAkun"
                                required="required"
                                placeholder="Input Nama Akun.."
                                onChange={handleAddFormChange}
                            />
                        </div>
                        <button type="submit">Add</button>
                    </form>

                    <form className="formBottom">
                        <Table
                            columns={columns}
                            dataSource={btbs}
                            scroll={{
                                x: 1500,
                            }}
                            sticky
                        />
                        <Modal
                            title="Edit BTB"
                            visible={isEditing}
                            okText="Save"
                            onCancel={() => {
                                resetEditing();
                            }}
                            onOk={() => {
                                setBtbs((btbs) => {
                                    return btbs.map((btb) => {
                                        if (btb.id === editBtbId.id) {
                                            return editBtbId;
                                        } else {
                                            return btb;
                                        }
                                    });
                                });
                                resetEditing();
                            }}
                        >
                            <Editbtb
                            editBtbId={editBtbId}
                            setEditBtbId={setEditBtbId}
                            codes={codes}
                            />
                        </Modal>
                        {/* <table>
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Nama Barang</th>
                        <th>Stock</th>
                        <th>Qty</th>
                        <th>Lokasi Beli</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {npbs.map((npb) => (
                        <Fragment>
                            { editNpbId === npb.id ? (
                            <Editnpb 
                            editFormData={editFormData}
                            handleEditFormChange={handleEditFormChange}
                            handleCancleClick={handleCancleClick}
                            />
                            ) : (
                            <Readnpb 
                            npb={npb}
                            handleEditClick={handleEditClick} 
                            handleDeleteClick={handleDeleteClick}
                            />
                            )}
                        </Fragment>
                    ))}
                </tbody>
            </table> */}
                    </form>
                </div>

            </div>
        </>
    )
}

export default Btb