import "../transaksi.scss"
import { Table, Button } from 'antd';
import React, { useEffect, useState } from "react";
import axios from "axios";
import Cookies from "js-cookies";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";


const StockMove = () => {

    const [dataStockMove, setDataStockMove] = useState([])

    useEffect(() => {
        let headers =  {
            "Authorization" : "Bearer " + Cookies.getItem('access_token')
        }

        const fetchData = async () => {
            let result = await axios.get('http://127.0.0.1:5000/api/stockmoves',
            { headers }
            )
            .then(response => setDataStockMove(response.data.data.stockMoves))
            .catch(error => {
              if (error.response) {
                console.log(error.response);
              }
            });

        }
        fetchData()
    },[])

    const stockmoveColumn = [
        { dataIndex: "name", title: "Nama", key: 'name', fixed: 'left', width: 150 },
        { dataIndex: "picking_type_id", title: "Picking Type ID", key: 'picking_type_id', width: 150 },
        { dataIndex: "product_tmpl_id", title: "Produk", key: 'product_tmpl_id', width: 150 },
        { dataIndex: "state", title: "State", key: 'state', width: 150 },
        { dataIndex: "done", title: "Done", key: 'done', width: 150 },
        { dataIndex: "demand", title: "Demand", key: 'demand', width: 150 },
        { dataIndex: "location_id", title: "Lokasi", key: 'location_id', width: 150 },
        { dataIndex: "location_dest_id", title: "Destinasi", key: 'location_dest_id', width: 150 },
        { dataIndex: "date", title: "Date", key: 'date', width: 150 },
        {
            title: 'Action',
            key: 'id', 
            dataIndex: 'id',
            fixed: 'right',
            width: 150,
            render: () => {
                return (
                    <>
                        <EditOutlined
                            onClick={() => {
                                
                            }}
                        />
                        <DeleteOutlined
                            onClick={() => {
                                
                            }}
                            style={{ color: "red", marginLeft: 12 }}
                        />
                    </>
                );
            }
        },
    ]

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Stock Move
                <Button href="/sp/new" type="primary" style={{ fontWeight: "bold" }}>Stock Picking Baru</Button>
            </div>
            <Table
                dataSource={dataStockMove}
                columns={stockmoveColumn}
                scroll={{
                    x: 1500,
                }}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default StockMove