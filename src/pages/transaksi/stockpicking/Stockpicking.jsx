import "../transaksi.scss"
import { Table, Button } from 'antd';
import React, { useEffect, useContext } from "react";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { AppContext } from '../../../context/Appcontext'


const Stockpicking = () => {

    const { Function, State } = useContext(AppContext)
    const { fetchDataStockPicking } = Function
    const { dataStockPicking } = State

    useEffect(() => {
        fetchDataStockPicking()
    }, [])

    const stockpickingColumn = [
        { dataIndex: "name", title: "Nama", key: 'name', fixed: 'left', width: 150 },
        { dataIndex: "origin", title: "Origin", key: 'origin', width: 150 },
        { dataIndex: "state", title: "State", key: 'state', width: 150 },
        { dataIndex: "picking_type", title: "Picking Type", key: 'picking_type', width: 150 },
        { dataIndex: "created_uid", title: "User", key: 'created_uid', width: 150 },
        { dataIndex: "location_name", title: "Lokasi", key: 'location_name', width: 150 },
        { dataIndex: "location_dest_name", title: "Destinasi", key: 'location_dest_name', width: 150 },
        { dataIndex: "date", title: "Date", key: 'date', width: 150 },
        {
            title: 'Action',
            key: 'id',
            dataIndex: 'id',
            fixed: 'right',
            width: 150,
            render: (id) => {
                // {dataStockPicking.map((e) => {

                // })}
                return (
                    <>
                        <Button 
                        href={`/sp/${id}`} 
                        onClick={() => {this.setState({selected: id})}}
                        icon={<EditOutlined/>} 
                        style={{ border: "none", background: "none" }}
                        />
                        <Button 
                        href={"/sp/edit/:id"}
                        onClick={() => {this.setState({selected: id})}}
                        icon={
                            <DeleteOutlined
                            style={{ color: "red", marginLeft: 12 }}
                            />
                        } 
                        style={{ border: "none", background: "none" }}
                        />
                        
                    </>
                );
            }
        },
    ]

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Stock Picking
                <Button href="/sp/new" type="primary" style={{ fontWeight: "bold" }}>Stock Picking Baru</Button>
            </div>
            <Table
                dataSource={dataStockPicking}
                columns={stockpickingColumn}
                scroll={{
                    x: 1500,
                }}
            // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Stockpicking