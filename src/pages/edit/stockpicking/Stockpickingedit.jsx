// import "../new.scss"
import React, { useEffect, useContext } from "react";
import { Button, Form, Input, Space, DatePicker, Select, Typography } from 'antd';
// import { useParams } from 'react-router-dom'
import 'moment/locale/zh-cn';
import { PlusOutlined, MinusCircleOutlined } from "@ant-design/icons";

// import { Stockmovetable } from "../../new/stockpicking/Stockmovetable";
import { AppContext } from '../../../context/Appcontext'

const Stockpickingedit = ({ title }) => {

  const { Function, State } = useContext(AppContext)
  const { fetchDataPickingType, fetchDataLocation, fetchDataPupuk, fetchDataStockPickingById, option, StockPick, StockPickFailed } = Function
  const { dateFormat, dataPupuk, dataStockLocation, dataStockPicking, dataStockPickingEdit, dataStockPickingType, fetchStatus, setFetchStatus, id } = State
  const { Title } = Typography;

  useEffect(() => {

    if (fetchStatus) {
      fetchDataStockPickingById()
      fetchDataPickingType()
      fetchDataLocation()
      fetchDataPupuk()
      setFetchStatus(false)
    }


  }, [
    fetchStatus,
    setFetchStatus,
    fetchDataPickingType,
    fetchDataLocation,
    fetchDataPupuk,
    fetchDataStockPickingById
  ])

  console.log(dataStockPicking)

  return (
    <>
      <div className='new'>
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">

          <div className="stockPicking">
            <Form
              name="stockPickings"
              layout="vertical"
              onFinish={StockPick}
              onFinishFailed={StockPickFailed}
              autoComplete="off"
            >
              <div className="editTitle">
                <Title>{dataStockPicking.name}</Title>
              </div>

              <div className="left">
                <Form.Item
                  label="Origin :"
                  name='origin'
                >
                  <Input
                    placeholder="Origin"
                  />
                </Form.Item>
                <Form.Item
                  label="Picking Type :"
                  name='picking_type_id'
                >
                  <Select
                    placeholder="Picking Type"
                    options={dataStockPickingType.map(e => ({ label: e.name, value: e.id }))}
                  />
                </Form.Item>
                <Form.Item
                  label="Date :"
                  name='date'
                >
                  <DatePicker
                    format={dateFormat}
                  />
                </Form.Item>
              </div>

              <div className="right">
                <Form.Item
                  label="Location :"
                  name='location_id'
                >
                  <Select
                    options={dataStockLocation.map(e => ({ label: e.name, value: e.id }))}
                    placeholder="Location"
                  />
                </Form.Item>
                <Form.Item
                  label="Destination :"
                  name='location_dest_id'
                >
                  <Select
                    options={dataStockLocation.map(e => ({ label: e.name, value: e.id }))}
                    placeholder="Destination"
                  />
                </Form.Item>
              </div>

              <div className="stockMove">

                <Form.Item>
                  <Form.List name="stock_move_ids">
                    {(fields, { add, remove }) => (
                      <>
                        {fields.map((field) => (
                          <Space
                            key={field.key}
                            style={{
                              display: 'flex',
                              marginBottom: 8,
                            }}
                            align="baseline"
                          >
                            <Form.Item
                              {...field}
                              name={[field.name, 'product_tmpl_id']}
                              key={[field.key, 'product_tmpl_id']}
                              rules={[
                                {
                                  required: true,
                                  message: 'Missing product_tmpl_id',
                                },
                              ]}
                            >
                              <Select
                                options={dataPupuk.map(e => ({ label: e.name, value: e.id }))}
                                placeholder="product_tmpl_id"
                              />
                            </Form.Item>
                            <Form.Item
                              {...field}
                              name={[field.name, 'demand']}
                              key={[field.key, 'demand']}
                              rules={[
                                {
                                  required: true,
                                  message: 'Missing Demand',
                                },
                              ]}
                            >
                              <Input placeholder="Demand" />
                            </Form.Item>
                            <Form.Item
                              {...field}
                              name={[field.name, 'done']}
                              key={[field.key, 'done']}
                            >
                              <Select
                                options={option}
                                placeholder="Done"
                              />
                            </Form.Item>
                            <Form.Item
                              {...field}
                              name={[field.name, 'product_uom']}
                              key={[field.key, 'product_uom']}
                              rules={[
                                {
                                  required: true,
                                  message: 'Missing product_uom',
                                },
                              ]}
                            >
                              <Select
                                options={option}
                                placeholder="product_uom"
                              />
                            </Form.Item>
                            <MinusCircleOutlined onClick={() => remove(field.name)} />
                          </Space>
                        ))}
                        <Form.Item>
                          <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                            Add field
                          </Button>
                        </Form.Item>
                      </>
                    )}
                  </Form.List>
                </Form.Item>

              </div>

              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>

        </div>
      </div>
    </>
  )
}

export default Stockpickingedit