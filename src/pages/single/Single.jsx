import "./single.scss"
import Chart from "../../components/chart/Chart"
import List from "../../components/table/Table"
import { useParams } from "react-router-dom"

const Single = () => {
  const {id} = useParams()
  console.log(id)
  return (
    <>
      <div className="single">
        <div className="top">
          <div className="left">
            <div className="editButton">Edit</div>
            <h1 className="title">Information</h1>
            <div className="item">
              <img
                src="https://www.nicepng.com/png/full/138-1388174_login-account-icon.png"
                alt=""
                className="itemImg" />
              <div className="details">
                <h1 className="itemTitle">Jo</h1>
                <div className="detailItem">
                  <span className="itemKey">Email : </span>
                  <span className="itemValue">Jo@mail.com</span>
                </div>
                <div className="detailItem">
                  <span className="itemKey">Phone : </span>
                  <span className="itemValue">+62 5555555555</span>
                </div>
                <div className="detailItem">
                  <span className="itemKey">Address : </span>
                  <span className="itemValue">Jl. Pakmulang</span>
                </div>
                <div className="detailItem">
                  <span className="itemKey">Country : </span>
                  <span className="itemValue">Indonesia</span>
                </div>
              </div>
            </div>
          </div>
          <div className="right">
            <Chart aspect={3 / 1} title="User Spendings (Last 6 Months)" />
          </div>
        </div>
        <div className="bottom">
          <h1 className="title">Last Transaction</h1>
          {/* <List /> */}
        </div>
      </div>
    </>
  )
}

export default Single