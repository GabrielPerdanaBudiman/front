import "./login.scss"
import React, { useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import { AppContext } from '../../context/Appcontext'
import { Button, Form, Input, Tooltip } from 'antd';
import { InfoCircleOutlined, UserOutlined, LockOutlined } from '@ant-design/icons';


const Login = () => {
  const { Function } = useContext(AppContext)
  const { logon, logonFailed } = Function

  return (
    <>
      <div className="login">
        <div className="loginLeft">
          <h1>
            Lorem ipsum
          </h1>
          <h4>
            dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore
            et dolore magna aliqua.
          </h4>
        </div>
        <div className="loginRight">
          <div className="top">
            <h1>Login</h1>
            <h4>Input Username dan Password untuk Masuk.</h4>
          </div>
          <div className="bottom">
            <Form
              name='basic'
              initialValues={{
                remember: true,
              }}
              onFinish={logon}
              onFinishFailed={logonFailed}
              autoComplete="on"
            >
              <Form.Item
                name='userName'
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input
                  placeholder="Masukkan Username"
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  suffix={
                    <Tooltip title="Contoh : Admin">
                      <InfoCircleOutlined
                        style={{
                          color: 'rgba(0,0,0,.45)',
                        }}
                      />
                    </Tooltip>
                  } />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password
                  placeholder="Masukkan Password"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                />
              </Form.Item>

              {/* <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Checkbox>Remember me</Checkbox>
            </Form.Item> */}

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>

        </div>
      </div>
    </>
  )
}

export default Login