import "../master.scss"
import { Table } from 'antd';
import { kapasitasColumn, kapasitasRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";


const Kapasitasmaster = () => {

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Kapasitas Gudang
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={kapasitasRows}
                columns={kapasitasColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Kapasitasmaster