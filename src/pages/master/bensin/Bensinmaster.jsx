import "../master.scss"
import { Table } from 'antd';
import { bensinColumn, bensinRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";


const Bensinmaster = () => {

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Bensin
                <Link
                    to="/bensin/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={bensinRows}
                columns={bensinColumn}
            />
        </div>
    )
}

export default Bensinmaster