import "../master.scss"
import { Table } from 'antd';
import { wilayahColumn, wilayahRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";


const Wilayahmaster = () => {

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Wilayah
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={wilayahRows}
                columns={wilayahColumn}
                pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Wilayahmaster