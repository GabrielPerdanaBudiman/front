import "../master.scss"
import { Table } from 'antd';
import { gudangColumn, gudangRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";


const Gudangmaster = () => {

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Gudang
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={gudangRows}
                columns={gudangColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Gudangmaster