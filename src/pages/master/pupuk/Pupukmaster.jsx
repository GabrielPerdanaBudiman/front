import "../master.scss"
import { Table } from 'antd';
import { pupukColumn  } from "../../../datatablesource";
import { Link } from "react-router-dom";
import React, { useEffect, useContext } from "react";
import { AppContext } from '../../../context/Appcontext'


const Pupukmaster = () => {

    const { Function, State } = useContext(AppContext)
    const { fetchDataPupuk } = Function
    const { dataPupuk } = State

    useEffect(() => {
        fetchDataPupuk()
    },[])

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Pupuk
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={dataPupuk}
                columns={pupukColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Pupukmaster