import "../master.scss"
import { Table } from 'antd';
import { pemilikColumn } from "../../../datatablesource";
import { Link } from "react-router-dom";
import React, { useEffect, useContext } from "react";
import { AppContext } from '../../../context/Appcontext'

const Pemilikmaster = () => {

    const { Function, State } = useContext(AppContext)
    const { fetchDataPemilik } = Function
    const { dataPemilik } = State

    useEffect(() => {
        fetchDataPemilik()
    },[])

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Pemilik
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={dataPemilik}
                columns={pemilikColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Pemilikmaster