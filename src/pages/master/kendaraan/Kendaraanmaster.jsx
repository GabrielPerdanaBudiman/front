import "../master.scss"
import { Table } from 'antd';
import { kendaraanColumn, kendaraanRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";


const Kendaraanmaster = () => {

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Kendaraan
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={kendaraanRows}
                columns={kendaraanColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Kendaraanmaster