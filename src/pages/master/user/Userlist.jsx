import "../master.scss"
import { Table } from 'antd';
import { userColumn, userRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";
import React, { useEffect, useContext } from "react";
import { AppContext } from '../../../context/Appcontext'

const Userlist = () => {

    const { Function, State } = useContext(AppContext)
    const { fetchDataPupuk } = Function
    const { dataPupuk } = State

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                List User
                <Link
                    to="/users/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={userRows}
                columns={userColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Userlist