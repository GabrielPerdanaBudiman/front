import "../master.scss"
import { Table } from 'antd';
import { mitraColumn, mitraRows  } from "../../../datatablesource";
import { Link } from "react-router-dom";


const Mitramaster = () => {

    return (
        <div className="datatable">
            <div className="dataTableTitle">
                Master Mitra
                <Link
                    to="/pupuk/new"
                    className="link"
                >
                    Add New
                </Link>
            </div>
            <Table
                dataSource={mitraRows}
                columns={mitraColumn}
                // pagination={{ pageSize: 5 }}
            />
        </div>
    )
}

export default Mitramaster