// import { CopyOutlined } from '@ant-design/icons';
// import {
//   AutoComplete,
//   Button,
//   Cascader,
//   Col,
//   DatePicker,
//   Input,
//   InputNumber,
//   Row,
//   Select,
//   Tooltip,
// } from 'antd';
// import React from 'react';
// const { Option } = Select;
// const options = [
//   {
//     value: 'zhejiang',
//     label: 'Zhejiang',
//     children: [
//       {
//         value: 'hangzhou',
//         label: 'Hangzhou',
//         children: [
//           {
//             value: 'xihu',
//             label: 'West Lake',
//           },
//         ],
//       },
//     ],
//   },
//   {
//     value: 'jiangsu',
//     label: 'Jiangsu',
//     children: [
//       {
//         value: 'nanjing',
//         label: 'Nanjing',
//         children: [
//           {
//             value: 'zhonghuamen',
//             label: 'Zhong Hua Men',
//           },
//         ],
//       },
//     ],
//   },
// ];

// const App = () => (
//   <div className="site-input-group-wrapper">
//     <Input.Group size="large">
//       <Row gutter={8}>
//         <Col span={5}>
//           <Input defaultValue="0571" />
//         </Col>
//         <Col span={8}>
//           <Input defaultValue="26888888" />
//         </Col>
//       </Row>
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Input
//         style={{
//           width: '20%',
//         }}
//         defaultValue="0571"
//       />
//       <Input
//         style={{
//           width: '30%',
//         }}
//         defaultValue="26888888"
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Input
//         style={{
//           width: 'calc(100% - 200px)',
//         }}
//         defaultValue="https://ant.design"
//       />
//       <Button type="primary">Submit</Button>
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Input
//         style={{
//           width: 'calc(100% - 200px)',
//         }}
//         defaultValue="git@github.com:ant-design/ant-design.git"
//       />
//       <Tooltip title="copy git url">
//         <Button icon={<CopyOutlined />} />
//       </Tooltip>
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Select defaultValue="Zhejiang">
//         <Option value="Zhejiang">Zhejiang</Option>
//         <Option value="Jiangsu">Jiangsu</Option>
//       </Select>
//       <Input
//         style={{
//           width: '50%',
//         }}
//         defaultValue="Xihu District, Hangzhou"
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Input.Search
//         allowClear
//         style={{
//           width: '40%',
//         }}
//         defaultValue="0571"
//       />
//       <Input.Search
//         allowClear
//         style={{
//           width: '40%',
//         }}
//         defaultValue="26888888"
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Select defaultValue="Option1">
//         <Option value="Option1">Option1</Option>
//         <Option value="Option2">Option2</Option>
//       </Select>
//       <Input
//         style={{
//           width: '50%',
//         }}
//         defaultValue="input content"
//       />
//       <InputNumber />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Input
//         style={{
//           width: '50%',
//         }}
//         defaultValue="input content"
//       />
//       <DatePicker
//         style={{
//           width: '50%',
//         }}
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Input
//         style={{
//           width: '30%',
//         }}
//         defaultValue="input content"
//       />
//       <DatePicker.RangePicker
//         style={{
//           width: '70%',
//         }}
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Select defaultValue="Option1-1">
//         <Option value="Option1-1">Option1-1</Option>
//         <Option value="Option1-2">Option1-2</Option>
//       </Select>
//       <Select defaultValue="Option2-2">
//         <Option value="Option2-1">Option2-1</Option>
//         <Option value="Option2-2">Option2-2</Option>
//       </Select>
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Select defaultValue="1">
//         <Option value="1">Between</Option>
//         <Option value="2">Except</Option>
//       </Select>
//       <Input
//         style={{
//           width: 100,
//           textAlign: 'center',
//         }}
//         placeholder="Minimum"
//       />
//       <Input
//         className="site-input-split"
//         style={{
//           width: 30,
//           borderLeft: 0,
//           borderRight: 0,
//           pointerEvents: 'none',
//         }}
//         placeholder="~"
//         disabled
//       />
//       <Input
//         className="site-input-right"
//         style={{
//           width: 100,
//           textAlign: 'center',
//         }}
//         placeholder="Maximum"
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Select
//         defaultValue="Sign Up"
//         style={{
//           width: '30%',
//         }}
//       >
//         <Option value="Sign Up">Sign Up</Option>
//         <Option value="Sign In">Sign In</Option>
//       </Select>
//       <AutoComplete
//         style={{
//           width: '70%',
//         }}
//         placeholder="Email"
//         options={[
//           {
//             value: 'text 1',
//           },
//           {
//             value: 'text 2',
//           },
//         ]}
//       />
//     </Input.Group>
//     <br />
//     <Input.Group compact>
//       <Select
//         style={{
//           width: '30%',
//         }}
//         defaultValue="Home"
//       >
//         <Option value="Home">Home</Option>
//         <Option value="Company">Company</Option>
//       </Select>
//       <Cascader
//         style={{
//           width: '70%',
//         }}
//         options={options}
//         placeholder="Select Address"
//       />
//     </Input.Group>
//   </div>
// );

// export default App;

// import { Input, Tooltip } from 'antd';
// import React, { useState } from 'react';

// const formatNumber = (value) => new Intl.NumberFormat().format(value);

// const NumericInput = (props) => {
//   const { value, onChange } = props;

//   const handleChange = (e) => {
//     const { value: inputValue } = e.target;
//     const reg = /^-?\d*(\.\d*)?$/;

//     if (reg.test(inputValue) || inputValue === '' || inputValue === '-') {
//       onChange(inputValue);
//     }
//   }; // '.' at the end or only '-' in the input box.

//   const handleBlur = () => {
//     let valueTemp = value;

//     if (value.charAt(value.length - 1) === '.' || value === '-') {
//       valueTemp = value.slice(0, -1);
//     }

//     onChange(valueTemp.replace(/0*(\d+)/, '$1'));
//   };

//   const title = value ? (
//     <span className="numeric-input-title">{value !== '-' ? formatNumber(Number(value)) : '-'}</span>
//   ) : (
//     'Input a number'
//   );
//   return (
//     <Tooltip trigger={['focus']} title={title} placement="topLeft" overlayClassName="numeric-input">
//       <Input
//         {...props}
//         onChange={handleChange}
//         onBlur={handleBlur}
//         placeholder="Input a number"
//         maxLength={25}
//       />
//     </Tooltip>
//   );
// };

// const App = () => {
//   const [value, setValue] = useState('');
//   return (
//     <NumericInput
//       style={{
//         width: 120,
//       }}
//       value={value}
//       onChange={setValue}
//     />
//   );
// };

// export default App;

// import { DownOutlined, UpOutlined } from '@ant-design/icons';
// import { Button, Col, Form, Input, Row, Select } from 'antd';
// import React, { useState } from 'react';
// const { Option } = Select;

// const AdvancedSearchForm = () => {
//   const [expand, setExpand] = useState(false);
//   const [form] = Form.useForm();

//   const getFields = () => {
//     const count = expand ? 10 : 6;
//     const children = [];

//     for (let i = 0; i < count; i++) {
//       children.push(
//         <Col span={8} key={i}>
//           <Form.Item
//             name={`field-${i}`}
//             label={`Field ${i}`}
//             rules={[
//               {
//                 required: true,
//                 message: 'Input something!',
//               },
//             ]}
//           >
//             {i % 3 !== 1 ? (
//               <Input placeholder="placeholder" />
//             ) : (
//               <Select defaultValue="2">
//                 <Option value="1">1</Option>
//                 <Option value="2">
//                   longlonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglonglong
//                 </Option>
//               </Select>
//             )}
//           </Form.Item>
//         </Col>,
//       );
//     }

//     return children;
//   };

//   const onFinish = (values) => {
//     console.log('Received values of form: ', values);
//   };

//   return (
//     <Form
//       form={form}
//       name="advanced_search"
//       className="ant-advanced-search-form"
//       onFinish={onFinish}
//     >
//       <Row gutter={24}>{getFields()}</Row>
//       <Row>
//         <Col
//           span={24}
//           style={{
//             textAlign: 'right',
//           }}
//         >
//           <Button type="primary" htmlType="submit">
//             Search
//           </Button>
//           <Button
//             style={{
//               margin: '0 8px',
//             }}
//             onClick={() => {
//               form.resetFields();
//             }}
//           >
//             Clear
//           </Button>
//           <a
//             style={{
//               fontSize: 12,
//             }}
//             onClick={() => {
//               setExpand(!expand);
//             }}
//           >
//             {expand ? <UpOutlined /> : <DownOutlined />} Collapse
//           </a>
//         </Col>
//       </Row>
//     </Form>
//   );
// };

// const App = () => (
//   <div>
//     <AdvancedSearchForm />
//     <div className="search-result-list">Search Result List</div>
//   </div>
// );

// export default App;


// display: flex;
// flex-flow: row wrap;
// min-width: 0;

// .ant-form-item
// box-sizing: border-box;
// padding: 0;
// color: rgba(0,0,0,.85);
// font-size: 14px;
// font-variant: tabular-nums;
// line-height: 1.5715;
// list-style: none;
// font-feature-settings: "tnum";
// margin: 5px;
// vertical-align: top;
// transition: margin-bottom .3s linear 17ms;

import { Button, Checkbox, Form, Input } from 'antd';
import React from 'react';

const App = () => {
  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your username!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};




<Form.Item>
  <Form.List name="stock_move_ids">
    {(fields, { add, remove }) => (
      <>
        {fields.map((field) => (
          <Space
            key={field.key}
            style={{
              display: 'flex',
              marginBottom: 8,
            }}
            align="baseline"
          >
            <Form.Item
              {...field}
              name={[field.name, 'demand']}
              key={[field.key, 'demand']}
              rules={[
                {
                  required: true,
                  message: 'Missing Demand',
                },
              ]}
            >
              <Input placeholder="Demand" />
            </Form.Item>
            <Form.Item
              {...field}
              name={[field.name, 'done']}
              key={[field.key, 'done']}
            >
              <Select
                options={options}
                placeholder="Done"
              />
            </Form.Item>
            <Form.Item
              {...field}
              name={[field.name, 'product_uom']}
              key={[field.key, 'product_uom']}
              rules={[
                {
                  required: true,
                  message: 'Missing product_uom',
                },
              ]}
            >
              <Select
                options={options}
                placeholder="product_uom"
              />
            </Form.Item>
            <Form.Item
              {...field}
              name={[field.name, 'product_tmpl_id']}
              key={[field.key, 'product_tmpl_id']}
              rules={[
                {
                  required: true,
                  message: 'Missing product_tmpl_id',
                },
              ]}
            >
              <Select
                options={options}
                placeholder="product_tmpl_id"
              />
            </Form.Item>
            <MinusCircleOutlined onClick={() => remove(field.name)} />
          </Space>
        ))}
        <Form.Item>
          <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
            Add field
          </Button>
        </Form.Item>
      </>
    )}
  </Form.List>
</Form.Item>





{/* <Table
                dataSource={stock_move}
                pagination={false}
                footer={() => {
                    return (
                        <Form.Item>
                            <Button onClick={add}>
                                <PlusOutlined /> Add field
                            </Button>
                        </Form.Item>
                    );
                }}
            >
                <Column
                    dataIndex={"product_tmpl_id"}
                    title={"Nama Produk"}
                    render={(value, row, index) => {
                        return (
                            <Form.Item name={[index, "product_tmpl_id"]}>
                                <Select
                                    placeholder="Produk"
                                    options={dataPupuk.map(e => ({ label: e.name, value: e.id }))}
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    dataIndex={"demand"}
                    title={"Demand"}
                    render={(value, row, index) => {
                        // console.log(row);
                        return (
                            <Form.Item name={[index, "demand"]}>
                                <Input
                                    placeholder="Demand"
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    dataIndex={"done"}
                    title={"Done"}
                    render={(value, row, index) => {
                        return (
                            <Form.Item name={[index, "done"]}>
                                <Select
                                    placeholder="Done"
                                    options={option}
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    dataIndex={"product_uom"}
                    title={"product_uom"}
                    render={(value, row, index) => {
                        return (
                            <Form.Item name={[index, "product_uom"]}>
                                <Input
                                    placeholder="product_uom"
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    title={"Action"}
                    render={(value, row, index) => {
                        return (
                            <React.Fragment>
                                <Button
                                    icon={<MinusOutlined />}
                                    shape={"circle"}
                                    onClick={() => remove(row.name)}
                                />
                            </React.Fragment>
                        );
                    }}
                />
            </Table> */}

export default App;