import "../new.scss"
import React, { useState } from "react";

const New = ({ inputs, title }) => {

    const [file, setFile] = useState("");

  return (
    <>
      <div className='new'>
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          {/* <div className="left">
            <img
              src=
              {
                file ? URL.createObjectURL(file)
                  : "https://tefa.smkn1tarakan.sch.id/attachments/no-image.png"
              }
              alt=""
            />
          </div> */}
          <div className="right">
            <form>
              {/* <div className="formInput">
                <label htmlFor="file">
                  Upload Image : 
                </label>
                <input
                  type="file"
                  id="file"
                  style={{ display: "none" }}
                  onChange={(e) => setFile(e.target.files[0])}
                />
              </div> */}
                
                <div className="formInput">
                    <label>Jenis Bensin</label>
                    <input type="text" placeholder="Pertamax / Solar" />
                </div>

                <div className="formInput">
                    <label>Jenis Kendaraan</label>
                    <input type="text" placeholder="Jenis Kendaraan" />
                </div>

                <div className="formInput">
                    <label>Meter</label>
                    <input type="text" placeholder="Meter" />
                </div>

                <div className="formInput">
                    <label>Liter</label>
                    <input type="text" placeholder="Liter" />
                </div>

              <button>Send</button>

            </form>
          </div>
        </div>
      </div>
    </>
  )
}

export default New