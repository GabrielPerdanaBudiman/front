import "../new.scss"
import React from "react";
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Form, Input, Space, Select, InputNumber  } from 'antd';

const Pupuknew = ({ title }) => {

  //--------------------------------------------------------------------------------------
  const options = [
    {
      value: '1',
      label: 'True',
    },
    {
      value: '0',
      label: 'False',
    },
  ];

  const onFinish = (values) => {
    console.log('Received values of form:', values);
  };
  //--------------------------------------------------------------------------------------


  return (
    <>
      <div className='new'>
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            <Form.List name="products">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(({ key, name, ...restField }) => (
                    <Space
                      key={key}
                      style={{
                        display: 'flex',
                        marginBottom: 8,
                      }}
                      align="baseline"
                    >

                      <Form.Item
                        {...restField}
                        name={[name, 'name']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Input placeholder="Isi Nama" />
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'description']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Input placeholder="Isi Deskripsi" />
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'type']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Input placeholder="Input Type" maxLength={20} />
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'category_id']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Select
                          options={options}
                          placeholder="Pilih Kategori"
                        />
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'active']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Select
                          options={options}
                          placeholder="Aktivasi Data"
                        />
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'list_price']}
                        rules={[
                          {
                            required: true,
                            message: 'Isi Jumlah Harga',
                          },
                        ]}
                      >
                        <InputNumber placeholder="Harga"/>
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'volume']}
                        rules={[
                          {
                            required: true,
                            message: 'Isi Jumlah Volume',
                          },
                        ]}
                      >
                        <InputNumber placeholder="Volume"/>
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'weight']}
                        rules={[
                          {
                            required: true,
                            message: 'Isi Jumlah Berat',
                          },
                        ]}
                      >
                        <InputNumber placeholder="Berat"/>
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'uom_id']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Input placeholder="Isi ID UOM" maxLength={20} />
                      </Form.Item>

                      <Form.Item
                        {...restField}
                        name={[name, 'default_code']}
                        rules={[
                          {
                            required: true,
                            message: 'Missing data',
                          },
                        ]}
                      >
                        <Input placeholder="Isi Kode Default"/>
                      </Form.Item>

                      <MinusCircleOutlined onClick={() => remove(name)} />
                    </Space>
                  ))}
                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                      Add field
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  )
}

export default Pupuknew