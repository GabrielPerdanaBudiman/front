import React from "react";
import { Form, Input, Button, Table, Select } from "antd";
import { PlusOutlined, MinusOutlined } from "@ant-design/icons";

const { Column } = Table;

export const Stockmovetable = props => {
    const { stock_move_ids, add, remove, dataPupuk, option } = props;

    return (

        <>
            <Table
                dataSource={stock_move_ids}
                pagination={false}
                footer={() => {
                    return (
                        <Form.Item>
                            <Button onClick={add}>
                                <PlusOutlined /> Add field
                            </Button>
                        </Form.Item>
                    );
                }}
            >
                <Column
                    dataIndex={"product_tmpl_id"}
                    title={"Nama Produk"}
                    render={(index) => {
                        return (
                            <Form.Item name={[index, "product_tmpl_id"]}>
                                <Select
                                    placeholder="Produk"
                                    options={dataPupuk.map(e => ({ label: e.name, value: e.id }))}
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    dataIndex={"demand"}
                    title={"Demand"}
                    render={(value, row, index) => {
                        // console.log(row);
                        return (
                            <Form.Item name={[index, "demand"]}>
                                <Input
                                    placeholder="Demand"
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    dataIndex={"done"}
                    title={"Done"}
                    render={(value, row, index) => {
                        return (
                            <Form.Item name={[index, "done"]}>
                                <Select
                                    placeholder="Tujuan Akhir"
                                    options={option}
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    dataIndex={"product_uom"}
                    title={"product_uom"}
                    render={(value, row, index) => {
                        return (
                            <Form.Item name={[index, "product_uom"]}>
                                <Input
                                    placeholder="product_uom"
                                />
                            </Form.Item>
                        );
                    }}
                />
                <Column
                    title={"Action"}
                    render={(value, row, index) => {
                        return (
                            <React.Fragment>
                                <Button
                                    icon={<MinusOutlined />}
                                    shape={"circle"}
                                    onClick={() => remove(row.name)}
                                />
                            </React.Fragment>
                        );
                    }}
                />
            </Table>
        </>

    );
};
