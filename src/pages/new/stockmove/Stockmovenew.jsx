import React from "react"
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Form, Input, Space, Select } from 'antd';

const Stockmovenew = props => {

    const options = [
        {
          value: '1',
          label: 'True',
        },
        {
          value: '0',
          label: 'False',
        },
        {
          value: '7',
          label: 'product_tmpl_id'
        },
      ];


    return (
        <>
            <Form.List name={[props.fieldKey, "stock_move_ids"]}>
    {(moves, { add, remove }) => (
        <>
            {moves.map((move, index) => (
                <Space
                    key={move.key}
                    style={{
                        display: 'flex',
                        marginBottom: 8,
                    }}
                    align="baseline"
                >
                    <Form.Item
                        {...move}
                        name={[move.name, 'demand']}
                        key={index}
                        rules={[
                            {
                                required: true,
                                message: 'Missing Demand',
                            },
                        ]}
                    >
                        <Input placeholder="Demand" />
                    </Form.Item>
                    <Form.Item
                        {...move}
                        name={[move.name, 'done']}
                        key={index}
                    >
                        <Select
                            options={options}
                            placeholder="Done"
                        />
                    </Form.Item>
                    <Form.Item
                        {...move}
                        name={[move.name, 'product_uom']}
                        key={index}
                        rules={[
                            {
                                required: true,
                                message: 'Missing product_uom',
                            },
                        ]}
                    >
                        <Select
                            options={options}
                            placeholder="product_uom"
                        />
                    </Form.Item>
                    <Form.Item
                        {...move}
                        name={[move.name, 'product_tmpl_id']}
                        key={index}
                        rules={[
                            {
                                required: true,
                                message: 'Missing product_tmpl_id',
                            },
                        ]}
                    >
                        <Select
                            options={options}
                            placeholder="product_tmpl_id"
                        />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(move.name)} />
                </Space>
            ))}
            <Form.Item>
                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                    Add field
                </Button>
            </Form.Item>
        </>
    )}
</Form.List>
        </>
    )
}

export default Stockmovenew