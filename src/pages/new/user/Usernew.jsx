import "./usernew.scss"
import React, { useState } from "react";

const New = ({ inputs, title }) => {

    const [file, setFile] = useState("");

  return (
    <>
      <div className='new'>
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img
              src=
              {
                file ? URL.createObjectURL(file)
                  : "https://tefa.smkn1tarakan.sch.id/attachments/no-image.png"
              }
              alt=""
            />
          </div>
          <div className="right">
            <form>
              <div className="formInput">
                <label htmlFor="file">
                  Upload Image : 
                </label>
                <input
                  type="file"
                  id="file"
                  style={{ display: "none" }}
                  onChange={(e) => setFile(e.target.files[0])}
                />
              </div>
                
                <div className="formInput">
                    <label>Username</label>
                    <input type="text" placeholder="john_doe" />
                </div>

                <div className="formInput">
                    <label>Nama</label>
                    <input type="text" placeholder="John Doe" />
                </div>

                <div className="formInput">
                    <label>Email</label>
                    <input type="mail" placeholder="john_doe@mail.com" />
                </div>

                <div className="formInput">
                    <label>Phone</label>
                    <input type="text" placeholder="+62 1234567890" />
                </div>
                
                <div className="formInput">
                    <label>Password</label>
                    <input type="password" />
                </div>

                <div className="formInput">
                    <label>Address</label>
                    <input type="text" placeholder="Jl. Jakarta Blok A No.1" />
                </div>

                <div className="formInput">
                    <label>Country</label>
                    <input type="text" placeholder="Indonesia" />
                </div>

              <div className="selection">
                <label>Kelas Pengguna</label>
                <select name="" id="">
                  <option value="">Admin A</option>
                  <option value="">Admin B</option>
                  <option value="">Admin C</option>
                  <option value="">Admin D</option>
                </select>

                
              </div>
              <button>Send</button>

            </form>
          </div>
        </div>
      </div>
    </>
  )
}

export default New