import "./table.scss"
import { Table } from 'antd';
import { testColumn, testRows  } from "../../datatablesource";


const Maintable = () => {

    return (
        <div className="testtable">
            <Table
                dataSource={testRows}
                columns={testColumn}
                pagination={{ pageSize: 5 }}
            />
        </div>
    )

}

export default Maintable