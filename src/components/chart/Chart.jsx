import "./chart.scss"
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import data from "../../charts.json"
import { useState } from "react";



const Chart = ({ aspect, title }) => {
  // const FilterBar = (kantor)
  const [allData, setData] = useState(data)
  const [chartData, setChartData] = useState([])

  const handleInput  = event => {

    console.log(event.target.value)
    let value = event.target.value;

    setChartData(
      data[value]
    );
  };

  return (
    <div className="chart">
      <div className="title">{title}</div>
      <div>
        <label>Kantor</label>
        <select
          onChange={handleInput}
        >
          <option>Select</option>
          
          {Object.entries(allData).map((item, index) => (
            <option value={item[0]} key={index}>
              {item[0]}
            </option>
          ))}
        </select>
      </div> 
      <ResponsiveContainer width="100%" aspect={aspect}>
        <BarChart
          width={500}
          height={300}
          data={chartData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis
            type="number"
            domain={[0, 7000000]}
            tickCount={8}
            tick={[0, 1000000, 2000000, 3000000, 4000000, 5000000, 6000000, 7000000]}
          />
          <Tooltip />
          <Legend />
          <Bar dataKey="Masuk" fill="#3E7CBA" />
          <Bar dataKey="Keluar" fill="#B4473A" />
          <Bar dataKey="Stock" fill="#B1D843" />
        </BarChart>
      </ResponsiveContainer>
    </div>
  )
}

export default Chart