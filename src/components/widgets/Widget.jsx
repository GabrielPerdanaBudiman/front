import "./widget.scss"
import { DollarCircleOutlined, ImportOutlined, ExportOutlined, WalletOutlined, UpOutlined } from '@ant-design/icons';

const Widget = ({ type }) => {
    let data;

    //temp
    const amount = 100;
    const diff = 20;

    switch (type) {
        case "masuk":
            data = {
                title: "Total Item Masuk",
                isMoney: false,
                link: "Lihat Total Item Masuk",
                icon: <ImportOutlined
                    className="icon"
                    style={{
                        color: "crimson",
                        backgroundColor: "rgba(255, 0, 0, 0.2)"
                    }} />,
            };
            break;

        case "keluar":
            data = {
                title: "Total Item Keluar",
                isMoney: false,
                link: "Lihat Total Item Keluar",
                icon: <ExportOutlined
                className="icon"
                style={{
                    color: "goldenrod",
                    backgroundColor: "rgba(218, 165, 32, 0.2)"
                }}/>,
            };
            break;

        case "anggaran":
            data = {
                title: "Total Saldo Anggaran",
                isMoney: true,
                link: "Lihat Total Saldo Anggaran",
                icon: <WalletOutlined 
                className="icon"
                style={{
                    color: "green",
                    backgroundColor: "rgba(0, 128, 0, 0.2)"
                }}/>,
            };
            break;

        case "pengeluaran":
            data = {
                title: "Total Saldo Pengeluaran",
                isMoney: true,
                link: "Lihat Total Saldo Pengeluaran",
                icon: <DollarCircleOutlined 
                className="icon"
                style={{
                    color: "purple",
                    backgroundColor: "rgba(128, 0, 128, 0.2)",
                }}/>,
            };
            break;
        default:
            break;
    }


    return (
        <div className="widget">
            <div className="left">
                <span className="title">{data.title}</span>
                <span className="counter">{data.isMoney && "$"} {amount}</span>
                <span className="link">{data.link}</span>
            </div>
            <div className="right">
                <div className="percentage positive">
                    <UpOutlined />
                    {diff} %
                </div>
                {data.icon}
            </div>
        </div>
    )
}

export default Widget