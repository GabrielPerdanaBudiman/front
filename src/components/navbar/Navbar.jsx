import React from 'react'
import './navbar.scss'
import { Menu, Layout } from 'antd';
import { Link } from "react-router-dom";


const { Header } = Layout
const Navbar = () => {

  return (
    <>
      <Header className="header">
        <div className="left">
          <Link to="/" style={{ textDecoration: "none" }}>
            <span className="logo">Admin</span>
          </Link>
        </div>
        <div className="right">
          <h3 className='user'>Selamat Datang, Admin</h3>
          <img src="https://www.kindpng.com/picc/m/235-2350646_login-user-name-user-avatar-svg-hd-png.png" alt="" className="avatar" />
        </div>

      </Header>
    </>
  )
}

export default Navbar