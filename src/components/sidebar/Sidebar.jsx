import "./sidebar.scss"
import { Link } from "react-router-dom";
import 'antd/dist/antd.min.css';
import { Layout, Menu } from 'antd';
import { AppContext } from '../../context/Appcontext'
import React, { useContext } from 'react'
import { LineChartOutlined, UserOutlined, ShoppingCartOutlined, TableOutlined, UserSwitchOutlined } from "@ant-design/icons";

const { Sider } = Layout;

const Sidebar = () => {

    const { Function } = useContext(AppContext)
    const { handleLogout } = Function

    return (
        <>
            <div className="sidebar">
                <div className="center">
                    <Sider className="sider">
                        <Menu
                            mode="inline"
                            // defaultSelectedKeys={['1']}
                            // defaultOpenKeys={['sub1']}
                            style={{ height: '100%', borderRight: 0 }}
                        >
                            <Menu.Item icon={<LineChartOutlined/>} ><Link to={'/'}><span>Dashboard</span></Link></Menu.Item>
                            <Menu.Item icon={<UserOutlined/>} ><Link to={'/users'}><span>Users</span></Link></Menu.Item>
                            <Menu.SubMenu icon={<TableOutlined />} title="Master">
                                <Menu.Item><Link to={'/pupuk'}><span>Pupuk</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/pemilik'}><span>Pemilik</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/mitra'}><span>Mitra</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/wilayah'}><span>Wilayah</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/gudang'}><span>Gudang</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/kapasitas'}><span>Kapasitas Gudang</span></Link></Menu.Item>
                                {/* <Menu.Item><Link to={'/kendaraan'}><span>Kendaraan</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/bensin'}><span>BBM</span></Link></Menu.Item> */}
                            </Menu.SubMenu>
                            <Menu.SubMenu icon={<ShoppingCartOutlined />} title="Transaksi">
                                <Menu.Item><Link to={'/sp'}><span>Stock Picking</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/sm'}><span>Stock Moves</span></Link></Menu.Item>
                                {/* <Menu.Item><Link to={'/npb'}><span>NPB</span></Link></Menu.Item>
                                <Menu.Item><Link to={'/btb'}><span>BTB</span></Link></Menu.Item>
                                <Menu.Item>BKB</Menu.Item>
                                <Menu.Item>NPKB</Menu.Item> */}
                            </Menu.SubMenu>
                            <Menu.SubMenu icon={<UserSwitchOutlined />} title="Akun">
                                <Menu.Item><Link to={'/login'}><span>Login</span></Link></Menu.Item>
                                <Menu.Item i onClick={handleLogout}>Logout</Menu.Item>
                            </Menu.SubMenu>
                        </Menu>
                    </Sider>
                </div>
            </div>
        </>
    )
}

export default Sidebar