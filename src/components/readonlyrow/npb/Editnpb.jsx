import React from 'react'
import { Input } from 'antd';

const Editnpb = ({ editNpbId, setEditNpbId }) => {
    return (
        <>

            <Input
                placeholder='Input Kode Barang..'
                value={editNpbId?.kode}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, kode: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Nama Barang..'
                value={editNpbId?.namaBarang}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, namaBarang: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Jumlah Stok..'
                value={editNpbId?.stok}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, stok: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Part Number..'
                value={editNpbId?.partNumber}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, partNumber: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Pg/Pic..'
                value={editNpbId?.pgPic}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, pgPic: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Qty..'
                value={editNpbId?.qty}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, qty: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Koreksi..'
                value={editNpbId?.koreksi}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, koreksi: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Outstanding..'
                value={editNpbId?.outstanding}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, outstanding: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Terima..'
                value={editNpbId?.terima}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, terima: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Satuan..'
                value={editNpbId?.satuan}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, satuan: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Remark..'
                value={editNpbId?.remark}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, remark: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Lokasi Beli..'
                value={editNpbId?.lokasiBeli}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, lokasiBeli: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Keterangan..'
                value={editNpbId?.keterangan}
                onChange={(e) => {
                    setEditNpbId((npbs) => {
                        return { ...npbs, keterangan: e.target.value };
                    });
                }}
            />




            {/*                 
            <input 
                type="text"
                name="kode"
                required="required"
                placeholder="Input Kode Barang.."
                value={editFormData.kode}
                onChange={handleEditFormChange}
            ></input>

            
            <input
                type="text"
                name="namaBarang"
                required="required"
                placeholder="Input Nama Barang.."
                value={editFormData.namaBarang}
                onChange={handleEditFormChange}    
            ></input>

            
            <input 
                type="text"
                name="stok"
                required="required"
                placeholder="Input Jumlah Stok.."
                value={editFormData.stok}
                onChange={handleEditFormChange}
            ></input>

            
            <input 
                type="text"
                name="qty"
                required="required"
                placeholder="Input Qty.."
                value={editFormData.qty}
                onChange={handleEditFormChange}
            ></input>

            
            <input 
                type="text"
                name="lokasiBeli"
                required="required"
                placeholder="Input Lokasi Beli.."
                value={editFormData.lokasiBeli}
                onChange={handleEditFormChange}
            ></input>

            
            <input 
                type="text"
                name="keterangan"
                required="required"
                placeholder="Input Keterangan.."
                value={editFormData.keterangan}
                onChange={handleEditFormChange}
            ></input> */}

        </>
    )
}

export default Editnpb