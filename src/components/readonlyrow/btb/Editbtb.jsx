import React from 'react'
import { Input, Select } from 'antd';

const Editbtb = ({ editBtbId, setEditBtbId, codes }) => {
    return (
        <>

            <Select
                name="kode"
                required="required"
                style={{ width : 100}}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, kode: e.target.value };
                    });
                }}>
                <option value="">Pilih Kode...</option>
                {codes.map((kode, index) => {
                    return <option key={index} value={kode}>{kode}</option>
                })}
            </Select>

            <Input
                placeholder='Input Nama Barang..'
                value={editBtbId?.namaBarang}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, namaBarang: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Qty..'
                value={editBtbId?.qty}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, qty: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Satuan..'
                value={editBtbId?.satuan}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, satuan: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Harga Satuan..'
                value={editBtbId?.hargaSatuan}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, hargaSatuan: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Valuta..'
                value={editBtbId?.valuta}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, valuta: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Kurs..'
                value={editBtbId?.kurs}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, kurs: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Jumlah..'
                value={editBtbId?.jumlah}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, jumlah: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input NPB..'
                value={editBtbId?.npb}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, npb: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Remark..'
                value={editBtbId?.remark}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, remark: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Keterangan..'
                value={editBtbId?.keterangan}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, keterangan: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Akun..'
                value={editBtbId?.akun}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, akun: e.target.value };
                    });
                }}
            />

            <Input
                placeholder='Input Nama Akun..'
                value={editBtbId?.namaAkun}
                onChange={(e) => {
                    setEditBtbId((btbs) => {
                        return { ...btbs, namaAkun: e.target.value };
                    });
                }}
            />



            {/*                 
            <input 
                type="text"
                name="kode"
                required="required"
                placeholder="Input Kode Barang.."
                value={editFormData.kode}
                onChange={handleEditFormChange}
            ></input>

            
            <input
                type="text"
                name="namaBarang"
                required="required"
                placeholder="Input Nama Barang.."
                value={editFormData.namaBarang}
                onChange={handleEditFormChange}    
            ></input>

            
            <input 
                type="text"
                name="stok"
                required="required"
                placeholder="Input Jumlah Stok.."
                value={editFormData.stok}
                onChange={handleEditFormChange}
            ></input>

            
            <input 
                type="text"
                name="qty"
                required="required"
                placeholder="Input Qty.."
                value={editFormData.qty}
                onChange={handleEditFormChange}
            ></input>

            
            <input 
                type="text"
                name="lokasiBeli"
                required="required"
                placeholder="Input Lokasi Beli.."
                value={editFormData.lokasiBeli}
                onChange={handleEditFormChange}
            ></input>

            
            <input 
                type="text"
                name="keterangan"
                required="required"
                placeholder="Input Keterangan.."
                value={editFormData.keterangan}
                onChange={handleEditFormChange}
            ></input> */}

        </>
    )
}

export default Editbtb