import React from 'react'
import { Breadcrumb, Layout, PageHeader } from 'antd';
import Sidebar from "../components/sidebar/Sidebar"
import Navbar from "../components/navbar/Navbar"
import "./layoutmain.scss"


const items1 = ['Login', '2', '3'].map((key) => ({
    key,
    label: `nav ${key}`,
}));

const Layoutmain = (props) => {
    const { Header, Content, Sider } = Layout;
    return (
        <Layout>
            <Navbar />

            <Layout>
                <Sidebar />
                <Layout

                >
                    <PageHeader
                        theme="dark"
                        className="site-page-header"
                        onBack={() => null}
                        title="Title"
                        subTitle="This is a subtitle"
                    />
                    <Content
                        style={{
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        {props.content}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    )
}

export default Layoutmain