export const userInputs = [
    {
        id: 1,
        label: "Username",
        type: "text",
        placeholder: "john_doe"
    },
    {
        id: 2,
        label: "Nama",
        type: "text",
        placeholder: "John Doe"
    },
    {
        id: 3,
        label: "Email",
        type: "mail",
        placeholder: "john_doe@mail.com"
    },
    {
        id: 4,
        label: "Phone",
        type: "text",
        placeholder: "+62 1234567890"
    },
    {
        id: 5,
        label: "Password",
        type: "password"
    },
    {
        id: 6,
        label: "Address",
        type: "text",
        placeholder: "Jl. Pamulang Blok A No.1"
    },
    {
        id: 7,
        label: "Country",
        type: "text",
        placeholder: "Indonesia"
    }
];

export const pupukInputs = [
    {
        id: 1,
        label: "Title",
        type: "text",
        placeholder: "Pupuk A"
    },
    {
        id: 2,
        label: "Jenis Pupuk",
        type: "text",
        placeholder: "Jenis Pupuk"
    },
    {
        id: 3,
        label: "Satuan",
        type: "text",
        placeholder: "Kilogram"
    }
];

export const bensinInputs = [
    {
        id: 1,
        label: "Jenis Bensin",
        type: "text",
        placeholder: "Pertamax"
    },
    {
        id: 2,
        label: "Description",
        type: "text",
        placeholder: "Description"
    },
    {
        id: 3,
        label: "Category",
        type: "text",
        placeholder: "Computers"
    },
    {
        id: 4,
        label: "Price",
        type: "text",
        placeholder: "1000000000"
    },
    {
        id: 5,
        label: "Stock",
        type: "text",
        placeholder: "in stock"
    }
];