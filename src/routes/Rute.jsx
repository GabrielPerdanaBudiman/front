import React from 'react'
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom"

//-----------------------------------------------
import Home from "../pages/home/Home";
import Login from "../pages/login/Login";
import Usermaster from '../pages/master/user/Userlist';
import Pupukmaster from '../pages/master/pupuk/Pupukmaster';
import Bensinmaster from '../pages/master/bensin/Bensinmaster';
import Pemilikmaster from '../pages/master/pemilik/Pemilikmaster';
import Mitramaster from '../pages/master/mitra/Mitramaster';
import Wilayahmaster from '../pages/master/wilayah/Wilayahmaster';
import Kapasitasmaster from '../pages/master/kapasitasgudang/Kapasitasmaster';
import Gudangmaster from '../pages/master/gudang/Gudangmaster';
import Kendaraanmaster from '../pages/master/kendaraan/Kendaraanmaster';
import Usernew from "../pages/new/user/Usernew";
import Pupuknew from "../pages/new/pupuk/Pupuknew";
import Bensinnew from "../pages/new/bensin/Bensinnew";
import Single from "../pages/single/Single";
// import { pupukInputs } from "../formSource";
import Layoutmain from "../layout/Layoutmain"
import Npb from '../pages/transaksi/npb/Npb';
import Btb from '../pages/transaksi/btb/Btb';
import Stockpicking from '../pages/transaksi/stockpicking/Stockpicking';
import Stockpickingnew from "../pages/new/stockpicking/Stockpickingnew";
import StockMove from '../pages/transaksi/stockmove/Stockmove';
import { AppProvider } from '../context/Appcontext';
import Stockpickingedit from '../pages/edit/stockpicking/Stockpickingedit';
// import Cookies from 'js-cookies';

//-----------------------------------------------


const Rute = () => {

  // const LoginRoute = ({...props}) => {

  //   if(Cookies.getItem('username') && Cookies.getItem('password') !== undefined){
  //     return <Navigate replace to='/'/>
  //   }else{
  //     return <Route {...props}/>
  //   }

  // }



  return (
    <>
      <Router>
        <AppProvider>
          <Routes>
            <Route path={'/'} element={<Layoutmain content={<Home />} />} />
            <Route path="/login" element={<Login />} />
            <Route path="/users" element={<Layoutmain content={<Usermaster />} />} />
            <Route path="/users/new" element={<Layoutmain content={<Usernew title="Tambah User Baru" />} />} />
            <Route path="/users/view/:id" element={<Layoutmain content={<Single />} />} />
            <Route path="/pupuk" element={<Layoutmain content={<Pupukmaster />} />} />
            {/* <Route path="/pupuk:pupukId" element={<Single />} /> */}
            <Route path="/pupuk/new" element={<Layoutmain content={<Pupuknew title="Tambah Pupuk Baru" />} />} />
            <Route path="/pemilik" element={<Layoutmain content={<Pemilikmaster />} />} />
            <Route path="/mitra" element={<Layoutmain content={<Mitramaster />} />} />
            <Route path="/wilayah" element={<Layoutmain content={<Wilayahmaster />} />} />
            <Route path="/gudang" element={<Layoutmain content={<Gudangmaster />} />} />
            <Route path="/kapasitas" element={<Layoutmain content={<Kapasitasmaster />} />} />
            <Route path="/kendaraan" element={<Layoutmain content={<Kendaraanmaster />} />} />
            <Route path="/bensin" element={<Layoutmain content={<Bensinmaster />} />} />
            <Route path="/bensin/new" element={<Layoutmain content={<Bensinnew title="Tambah User Baru" />} />} />
            <Route path="/sp" element={<Layoutmain content={<Stockpicking />} />} />
            <Route path="/sp/new" element={<Layoutmain content={<Stockpickingnew title="Tambah Stock Picking Baru" />} />} />
            <Route path="/sp/:id" element={<Layoutmain content={<Stockpickingedit title="Edit Stock Picking" />} />} />
            <Route path="/sm" element={<Layoutmain content={<StockMove />} />} />
            <Route path="/npb" element={<Layoutmain content={<Npb />} />} />
            <Route path="/btb" element={<Layoutmain content={<Btb />} />} />
          </Routes>
        </AppProvider>
      </Router>
    </>
  )
}

export default Rute